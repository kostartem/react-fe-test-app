import React from "react";
import {SearchInputWrapper, StyledButton, StyledInput} from "./search-input.styled";
import {SearchOutlined} from "@ant-design/icons";

export interface Props {
    setSearchStr: any
    searchFunction: any
    loading: boolean
}

export const SearchInputComponent: React.FC<Props> = ({setSearchStr, searchFunction, loading}) => {
    return (
        <SearchInputWrapper>
            <StyledInput onChange={(event: any) => setSearchStr(event.target.value)}
                         data-cy='search-input'
                         placeholder="Type something here"
            />
            <StyledButton icon={<SearchOutlined/>}
                          type="primary"
                          loading={loading}
                          data-cy='search-button'
                          onClick={searchFunction}>Search</StyledButton>
        </SearchInputWrapper>);
};

export const SearchInput = SearchInputComponent;
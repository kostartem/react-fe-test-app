describe("main-page", () => {

    beforeEach(() => {
        cy.visit("");
    })

    it("open home page", () => {
        cy.get("[data-cy='home-page-title']")
            .should("exist")
            .should("have.text", "Candidates Search");

        cy.get("[data-cy='search-button']")
            .should("exist")
            .should("have.text", "Search");

        cy.get("[data-cy='search-result-item']")
            .should('have.length.at.least', 1)
    });

    it("search should return results", () => {

        cy.get("[data-cy='search-input']")
            .type("Java");

        cy.get("[data-cy='search-button']")
            .click();

        cy.get("[data-cy='search-result-item']")
            .should('have.length.at.least', 1)
    });

    it("search should not return results", () => {

        cy.get("[data-cy='search-input']")
            .type("Not existing text");

        cy.get("[data-cy='search-button']")
            .click();

        cy.get("[data-cy='search-result-item']")
            .should('not.exist');
    });

    it("open details should show phone number and skills", () => {

        cy.get("[data-cy='search-result-item']")
            .first().find("[data-cy='phone-number']")
            .should('not.exist');

        cy.get("[data-cy='search-result-item']")
            .first().find("[data-cy='experience']")
            .should('not.exist');

        cy.get("[data-cy='search-result-item']")
            .find("[data-cy='view-details-button']")
            .should('exist').first().click();

        cy.get("[data-cy='search-result-item']")
            .first()
            .find("[data-cy='phone-number']")
            .should('exist');

        cy.get("[data-cy='search-result-item']")
            .first()
            .find("[data-cy='experience']")
            .should('exist');
    });
});
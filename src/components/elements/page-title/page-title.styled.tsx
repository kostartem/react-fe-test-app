import styled from 'styled-components';

export const TitleComponentWrapper = styled.div`
  display: flex;
  margin: 0 auto 10px auto;
  color: #0d0346;
  width: 1280px;
  max-width: 90%;
  font-weight: 400;
  font-size: 44px;
  line-height: 88px;
  padding: 10px 30px;
  border: 1px solid #000000;
  border-top: none;
  border-radius: 0 0 8px 8px;
  box-shadow: 0 9px 54px rgba(24, 6, 125, 0.08);
`

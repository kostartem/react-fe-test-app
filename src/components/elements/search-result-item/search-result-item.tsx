import React, {useState} from "react";
import {
    CandidateSalaryWrapper,
    CandidateTitleWrapper, DetailedInfoWrapper, ExperiencesWrapper,
    SearchResultItemWrapper, SkillWrapper, StyledButton,
    StyledImg,
    SummaryDetailsWrapper
} from "./search-result-item.styled";
import {CandidateSummary, TCandidatesDetails} from "../../../types/Candidates";
import {getCandidateDetails} from "../../../api/candidates-api/getCandidateDetails";

export interface Props {
    candidate: CandidateSummary
}

export const SearchResultItemComponent: React.FC<Props> = ({candidate}) => {
    const [showDetails, setShowDetails] = useState<boolean>(false);
    const [candidateDetails, setCandidateDetails] = useState<TCandidatesDetails>({} as TCandidatesDetails);

    const getCandidateDetailsFunction = (id: string) => {
        if (!showDetails) {
            getCandidateDetails(id).then(
                (response) => {
                    setCandidateDetails(response?.data)
                    setShowDetails(true)
                }
            )
        }
    }

    return (
        <SearchResultItemWrapper data-cy="search-result-item">
            <StyledImg src={candidate.avatarId} loading="lazy"/>
            <SummaryDetailsWrapper>
                <div className="candidate-top-block">
                    <div className="add-info">
                        <CandidateTitleWrapper>
                            {candidate.title}
                        </CandidateTitleWrapper>
                        <CandidateSalaryWrapper>
                            {candidate.salaryExpectations}
                        </CandidateSalaryWrapper>
                    </div>

                    <div className="full-name">
                        {candidate.firstName} {candidate.lastName}
                    </div>
                </div>
                <div className="description">{candidate.details}</div>
                <p>Skills:</p>
                <div className="skills">
                    {
                        candidate.skills.map((skill, index) =>
                            <div key={index} className="skill">
                                <SkillWrapper>{skill.name}</SkillWrapper>
                            </div>)
                    }
                </div>
                <div className="details">
                    <StyledButton data-cy="view-details-button"
                                  onClick={() => showDetails ? setShowDetails(false) :
                                      getCandidateDetailsFunction(candidate.id)}>
                        {showDetails ? "Hide details" : "View details"}
                    </StyledButton>
                </div>
                {
                    showDetails &&
                    <DetailedInfoWrapper>
                        {candidateDetails.phoneNumber &&
                            <div className="phoneNumber" data-cy="phone-number">
                                Phone number: {candidateDetails.phoneNumber}
                            </div>
                        }

                        {candidateDetails?.experiences?.length &&
                            <ExperiencesWrapper data-cy="experience">
                                <p>Experiences:</p>
                                {candidateDetails.experiences.map((experience, index) =>
                                    <div key={index} className="experience">
                                        <>
                                            <div className="experience-title">{experience.title}</div>
                                            {experience.fromDate.toString().substring(0, 7)} - {experience.toDate.toString().substring(0, 7)}</>
                                    </div>)
                                }
                            </ExperiencesWrapper>
                        }
                    </DetailedInfoWrapper>
                }


            </SummaryDetailsWrapper>
        </SearchResultItemWrapper>);
};

export const SearchResultItem = SearchResultItemComponent;
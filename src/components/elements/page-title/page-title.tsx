import React from "react";
import {TitleComponentWrapper} from "./page-title.styled";

export interface Props {
    message: string
}

export const PageTitleComponent: React.FC<Props> = ({message}) => {
    return (
        <TitleComponentWrapper data-cy="home-page-title">
            {message}
        </TitleComponentWrapper>);
};

export const PageTitle = PageTitleComponent;
export type TFilterCandidatesResponse = {
    data: TFilterCandidates
};

export type TFilterCandidates = {
    data: CandidateSummary[]
    totalPages: number
};

export type CandidateSummary = {
    id: string
    firstName: string
    avatarId: string
    lastName: string
    details: string
    title: string
    salaryExpectations: number

    skills: Skill[]
}

export type Skill = {
    name: string,
    level: TSkillLevel
}

export enum TSkillLevel {
    BEGINNER = "BEGINNER",
    INTERMEDIATE = "INTERMEDIATE",
    ADVANCED = "ADVANCED",
    EXPERT = "EXPERT"
}

export enum TLanguageLevel {
    BASIC = "BASIC",
    CONVERSATIONAL = "CONVERSATIONAL",
    FLUENT = "FLUENT",
    NATIVE = "NATIVE"
}

export type TCandidatesDetailsResponse = {
    data: TCandidatesDetails
};

export type TCandidatesDetails = {
    data: TFilterCandidates
    firstName: string
    avatarId: string
    lastName: string
    details: string
    title: string
    salaryExpectations: number
    phoneNumber: string
    skills: Skill[]
    spokenLanguages: SpokenLanguage[] | null
    experiences: Experience[] | null
};

export type SpokenLanguage = {
    languageId: string,
    level: TLanguageLevel
}

export type Experience = {
    title: string,
    fromDate: string | Date;
    toDate: string | Date;
}
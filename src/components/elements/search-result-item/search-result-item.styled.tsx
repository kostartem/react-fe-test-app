import styled from 'styled-components';
import {Button} from "antd";

export const SearchResultItemWrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  min-height: 250px;
  max-width: 1000px;
  margin: 5px 0;
  padding: 10px;
  box-shadow: 0 9px 54px rgba(24, 6, 125, 0.08);
  border-radius: 16px;

  .add-info {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin-bottom: 10px;
  }

  .full-name {
    font-size: 26px;
    font-weight: 500;
    line-height: 28px;
    color: #25178f;
    margin-bottom: 10px;
  }

  .description {
    font-size: 16px;
    font-weight: 400;
    line-height: 18px;
    color: #7e7c7c;
    margin-bottom: 20px;
  }

  .skills {
    display: flex;
    flex-direction: row;
  }

  .details {
    margin-left: auto;
  }
`

export const SummaryDetailsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  min-height: 250px;
  max-width: 1000px;
  margin: 5px 0;
  padding: 10px;
`

export const CandidateTitleWrapper = styled.div`
  font-size: 16px;
  font-weight: 600;
  line-height: 28px;
  color: black;
`

export const SkillWrapper = styled.div`
  border-radius: 6px;
  border: 1px solid rgb(232, 230, 242);
  background: rgb(232, 230, 242);
  padding: 4px 8px;
  align-items: center;
  column-gap: 4px;
  margin: 0 4px;
  color: rgb(93, 81, 164);
  font-size: 14px;
  font-weight: 400;
  display: flex;
`

export const CandidateSalaryWrapper = styled.div`
  font-size: 32px;
  font-weight: 600;
  line-height: 28px;
  color: #25178f;
`

export const DetailedInfoWrapper = styled.div`
  font-size: 16px;
  font-weight: 400;
  line-height: 18px;
  color: #7e7c7c;
  margin-bottom: 20px;
`

export const ExperiencesWrapper = styled.div`
  color: #000000;

  .experience {
    display: flex;
    flex-direction: row;
  }

  .experience-title {
    font-weight: 600;
    margin-right: 20px;
  }
`

export const StyledImg = styled.img`
  max-width: 200px;
  max-height: 200px;
  margin: 0 10px 10px 0;
  border-radius: 16px;
`;

export const StyledButton = styled(Button)`
  background-color: #0d0346;
  margin-right: 10px;
  color: white;
`

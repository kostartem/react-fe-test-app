import React from 'react';
import './components/assets/variables.less';
import {HonePageComponent} from "./components/pages/home-page/home-page";

function App() {
  return (
      <div className="App">
          <HonePageComponent/>
      </div>
  );
}

export default App;

import styled from 'styled-components';
import {Pagination, Spin} from "antd";
import {SearchInput} from "../../elements/search-input";

export const HomePageComponentWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  max-width: 1200px;
  margin-right: auto;
  margin-left: auto;
`

export const CandidatesListWrapper = styled.div`
  width: 1000px;
  max-width: 100%;
  display: flex;
  flex-direction: column;
  margin-top: 10px;
  margin-right: auto;
  margin-left: auto;
`

export const StyledSpin = styled(Spin)`
  margin: 100px 0;
`

export const StyledSearchInput = styled(SearchInput)`
  width: 1000px;
  max-width: 100%;
`

export const StyledPagination = styled(Pagination)`
  margin: 20px auto 40px auto;
`

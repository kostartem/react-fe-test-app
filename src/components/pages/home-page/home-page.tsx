import React, {useEffect, useState} from "react";
import {
    HomePageComponentWrapper,
    StyledPagination,
    CandidatesListWrapper,
    StyledSpin,
    StyledSearchInput
} from "./home-page.styled";
import {PageTitleComponent} from "../../elements/page-title/page-title";
import {CandidateSummary} from "../../../types/Candidates";
import {filterCandidates} from "../../../api/candidates-api/filterCandidates";
import {SearchResultItem} from "../../elements/search-result-item";

export interface Props {
}

export const HonePageComponent: React.FC<Props> = () => {
    const [candidates, setCandidates] = useState<CandidateSummary[]>([] as CandidateSummary[]);
    const [page, setPage] = useState<number>(0);
    const [loading, setLoading] = useState<boolean>(true);
    const [total, setTotal] = useState<number>(0);
    const [searchString, setSearchString] = useState<string>("");


    const getCandidatesResult = () => {
        setLoading(true);
        filterCandidates(page, searchString).then(
            (response) => {
                setTotal(response?.data?.totalPages)
                setCandidates(response?.data?.data)
                setLoading(false);
            }
        )
    }

    useEffect(() => {
        getCandidatesResult();
    }, [page]);

    return (<HomePageComponentWrapper>

        <PageTitleComponent message="Candidates Search"/>
        <StyledSearchInput setSearchStr={setSearchString}
                           searchFunction={getCandidatesResult}
                           loading={loading}
        />

        {
            loading ? <StyledSpin/> : <CandidatesListWrapper>
                {!!candidates?.length ? candidates?.map((candidate) => {
                    return <SearchResultItem candidate={candidate}/>
                }) : <div>No Results</div>} </CandidatesListWrapper>
        }

        <StyledPagination
            onChange={(pageNumber) => {
                setPage(pageNumber - 1)
            }}
            pageSize={10}
            total={total}
        />

    </HomePageComponentWrapper>);
};

export const HomePage = HonePageComponent;
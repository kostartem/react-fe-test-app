import {CANDIDATES} from "../endpoints";
import axiosClient from "../../config/axiosConfig";
import {TFilterCandidatesResponse} from "../../types/Candidates";

export const filterCandidates = async (page: number, searchStr: string, size: number = 10): Promise<TFilterCandidatesResponse> => {
    return axiosClient.get(CANDIDATES(), {
        params: {
            size,
            page,
            searchStr
        }
    });
}


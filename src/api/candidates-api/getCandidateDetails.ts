import {CANDIDATES_DETAILS} from "../endpoints";
import axiosClient from "../../config/axiosConfig";
import {TCandidatesDetailsResponse} from "../../types/Candidates";

export const getCandidateDetails = async (id: string): Promise<TCandidatesDetailsResponse> => {
    return axiosClient.get(CANDIDATES_DETAILS(id));
}


export const CANDIDATES = () => `/v1/candidates`;

export const CANDIDATES_DETAILS = (id: string) => `/v1/candidates/${id}`;

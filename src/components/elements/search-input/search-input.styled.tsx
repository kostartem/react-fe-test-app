import styled from 'styled-components';
import {Button, Input} from "antd";

export const SearchInputWrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 500px;
  margin: 30px 30px 30px auto;
`

export const StyledInput = styled(Input)`
  margin: 0 20px;
  border-color: #0d0346;
`

export const StyledButton = styled(Button)`
  background-color: #0d0346;
  margin-right: 60px;

`